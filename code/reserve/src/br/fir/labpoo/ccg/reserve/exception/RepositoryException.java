package br.fir.labpoo.ccg.reserve.exception;

public class RepositoryException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public RepositoryException(String arg0) {
		super(arg0);
		// TODO Auto-generated constructor stub
	}

	public RepositoryException(String arg0, Throwable arg1) {
		super(arg0, arg1);
		// TODO Auto-generated constructor stub
	}

	public RepositoryException(Throwable arg0) {
		super(arg0);
		// TODO Auto-generated constructor stub
	}
}