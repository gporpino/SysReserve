/**
 *
 */
package br.fir.labpoo.ccg.reserve.repository.memoria;

import java.util.TreeMap;

import br.fir.labpoo.ccg.reserve.model.Servico;

/**
 * @author cristiano
 *
 */
public class RepositorioServicoMemoria extends IRepositoryMemoryImpl<Servico> {

	/**
	 * @param repositorioServico
	 */
	public RepositorioServicoMemoria() {
		super( new TreeMap<Integer, Servico>());
	}
}
