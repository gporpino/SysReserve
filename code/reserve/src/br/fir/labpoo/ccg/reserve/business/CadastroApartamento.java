/**
 *
 */
package br.fir.labpoo.ccg.reserve.business;

import br.fir.labpoo.ccg.reserve.exception.RepositoryException;
import br.fir.labpoo.ccg.reserve.factory.RepositoryFactory;
import br.fir.labpoo.ccg.reserve.model.Apartamento;

/**
 * @author clelia
 *
 */
public class CadastroApartamento extends Cadastro<Apartamento> {

	public CadastroApartamento() throws RepositoryException{
		super(RepositoryFactory.getInstance().getRepositoryApartamento());
	}

}
