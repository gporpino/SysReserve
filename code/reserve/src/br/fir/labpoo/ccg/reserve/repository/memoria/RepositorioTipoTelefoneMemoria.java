/**
 *
 */
package br.fir.labpoo.ccg.reserve.repository.memoria;

import java.util.TreeMap;

import br.fir.labpoo.ccg.reserve.model.TipoTelefone;

/**
 * @author cristiano
 *
 */
public class RepositorioTipoTelefoneMemoria extends IRepositoryMemoryImpl<TipoTelefone> {

	/**
	 * @param repositorioTipoTelefone
	 */
	public RepositorioTipoTelefoneMemoria() {
		super( new TreeMap<Integer, TipoTelefone>());
	}
	
}
