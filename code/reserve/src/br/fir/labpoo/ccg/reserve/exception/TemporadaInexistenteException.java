package br.fir.labpoo.ccg.reserve.exception;

public class TemporadaInexistenteException extends RuntimeException {

private static final long serialVersionUID = 1L;

public TemporadaInexistenteException(){
		super("N�o foi encontrada nenhuma temporada para o per�odo solicitado.");
	}

}
