/**
 *
 */
package br.fir.labpoo.ccg.reserve.repository.memoria;

import java.util.TreeMap;

import br.fir.labpoo.ccg.reserve.model.UnidadeFederativa;

/**
 * @author cristiano
 *
 */
public class RepositorioUnidadeFederativaMemoria extends IRepositoryMemoryImpl<UnidadeFederativa> {

	/**
	 * @param repositorioUnidadeFederativa
	 */
	public RepositorioUnidadeFederativaMemoria() {
		super( new TreeMap<Integer, UnidadeFederativa>());
	}
}
