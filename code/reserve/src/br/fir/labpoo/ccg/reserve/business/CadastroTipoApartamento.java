/**
 *
 */
package br.fir.labpoo.ccg.reserve.business;

import br.fir.labpoo.ccg.reserve.exception.RepositoryException;
import br.fir.labpoo.ccg.reserve.factory.RepositoryFactory;
import br.fir.labpoo.ccg.reserve.model.TipoApartamento;

/**
 * @author clelia
 *
 */
public class CadastroTipoApartamento extends Cadastro<TipoApartamento> {

	public CadastroTipoApartamento() throws RepositoryException{
		super(RepositoryFactory.getInstance().getRepositoryTipoApartamento());
	}

}
