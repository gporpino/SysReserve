package br.fir.labpoo.ccg.reserve.util.shutdown;

public interface ShutdownListener {

	public void onShutdown() throws Throwable;
}
