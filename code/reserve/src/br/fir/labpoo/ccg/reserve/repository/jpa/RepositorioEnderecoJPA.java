/**
 * 
 */
package br.fir.labpoo.ccg.reserve.repository.jpa;

import br.fir.labpoo.ccg.reserve.exception.EntradaInexistenteException;
import br.fir.labpoo.ccg.reserve.exception.RepositoryException;
import br.fir.labpoo.ccg.reserve.model.Endereco;

/**
 * @author cristiano
 *
 */
public class RepositorioEnderecoJPA extends IRepositoryJPAImpl<Endereco> {

	public RepositorioEnderecoJPA() throws RepositoryException {
		super();
		// TODO Auto-generated constructor stub
	}

	/* (non-Javadoc)
	 * @see repositorio.endereco.IntRepositorioEndereco#alterarEndereco(negocio.endereco.Endereco)
	 */
	public void alterarEndereco(Endereco endereco)
			throws EntradaInexistenteException {
		// TODO Auto-generated method stub

	}

	/* (non-Javadoc)
	 * @see repositorio.endereco.IntRepositorioEndereco#consultarEnderecoCodigo(int)
	 */
	public Endereco consultarEnderecoCodigo(int codigoEndereco) {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see repositorio.endereco.IntRepositorioEndereco#excluirEndereco(negocio.endereco.Endereco)
	 */
	public void excluirEndereco(Endereco endereco)
			throws EntradaInexistenteException {
		// TODO Auto-generated method stub

	}

	/* (non-Javadoc)
	 * @see repositorio.endereco.IntRepositorioEndereco#inserirEndereco(negocio.endereco.Endereco)
	 */
	public void inserirEndereco(Endereco endereco) {
		// TODO Auto-generated method stub

	}

}
