/**
 *
 */
package br.fir.labpoo.ccg.reserve.repository.jpa;

import java.util.Date;
import java.util.Set;

import br.fir.labpoo.ccg.reserve.exception.EntradaInexistenteException;
import br.fir.labpoo.ccg.reserve.exception.RepositoryException;
import br.fir.labpoo.ccg.reserve.model.Apartamento;
import br.fir.labpoo.ccg.reserve.model.Reserva;

/**
 * @author cristiano
 *
 */
public class RepositorioReservaJPA extends IRepositoryJPAImpl<Reserva> {

	public RepositorioReservaJPA() throws RepositoryException {
		super();
		// TODO Auto-generated constructor stub
	}

	/* (non-Javadoc)
	 * @see repositorio.movimentacao.reserva.IntRepositorioReserva#alterarReserva(negocio.movimentacao.reserva.Reserva)
	 */
	public void alterarReserva(Reserva reserva)
			throws EntradaInexistenteException {
		// TODO Auto-generated method stub

	}

	/* (non-Javadoc)
	 * @see repositorio.movimentacao.reserva.IntRepositorioReserva#consultarReservaCodigo(int)
	 */
	public Reserva consultarReservaCodigo(int codigoReserva) {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see repositorio.movimentacao.reserva.IntRepositorioReserva#excluirReserva(negocio.movimentacao.reserva.Reserva)
	 */
	public void excluirReserva(Reserva reserva)
			throws EntradaInexistenteException {
		// TODO Auto-generated method stub

	}

	/* (non-Javadoc)
	 * @see repositorio.movimentacao.reserva.IntRepositorioReserva#inserirReserva(negocio.movimentacao.reserva.Reserva)
	 */
	public void inserirReserva(Reserva reserva) {
		// TODO Auto-generated method stub

	}

	public Set<Apartamento> listApartamentosHospedagem(Date inicio, Date fim) {
		// TODO Auto-generated method stub
		return null;
	}

	public double getValorReserva(Reserva reserva) {
		// TODO Auto-generated method stub
		return 0;
	}

}
