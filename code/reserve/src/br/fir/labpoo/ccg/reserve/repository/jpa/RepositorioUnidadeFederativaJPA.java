/**
 * 
 */
package br.fir.labpoo.ccg.reserve.repository.jpa;

import br.fir.labpoo.ccg.reserve.exception.EntradaInexistenteException;
import br.fir.labpoo.ccg.reserve.exception.RepositoryException;
import br.fir.labpoo.ccg.reserve.model.UnidadeFederativa;

/**
 * @author cristiano
 *
 */
public class RepositorioUnidadeFederativaJPA extends IRepositoryJPAImpl<UnidadeFederativa> {

	public RepositorioUnidadeFederativaJPA() throws RepositoryException {
		super();
		// TODO Auto-generated constructor stub
	}

	/* (non-Javadoc)
	 * @see repositorio.unidadeFederativa.IntRepositorioUnidadeFederativa#alterarUnidadeFederativa(negocio.unidadeFederativa.UnidadeFederativa)
	 */
	public void alterarUnidadeFederativa(UnidadeFederativa unidadeFederativa)
			throws EntradaInexistenteException {
		// TODO Auto-generated method stub

	}

	/* (non-Javadoc)
	 * @see repositorio.unidadeFederativa.IntRepositorioUnidadeFederativa#consultarUnidadeFederativaCodigo(int)
	 */
	public UnidadeFederativa consultarUnidadeFederativaCodigo(
			int codigoUnidadeFederativa) {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see repositorio.unidadeFederativa.IntRepositorioUnidadeFederativa#excluirUnidadeFederativa(negocio.unidadeFederativa.UnidadeFederativa)
	 */
	public void excluirUnidadeFederativa(UnidadeFederativa unidadeFederativa)
			throws EntradaInexistenteException {
		// TODO Auto-generated method stub

	}

	/* (non-Javadoc)
	 * @see repositorio.unidadeFederativa.IntRepositorioUnidadeFederativa#inserirUnidadeFederativa(negocio.unidadeFederativa.UnidadeFederativa)
	 */
	public void inserirUnidadeFederativa(UnidadeFederativa unidadeFederativa) {
		// TODO Auto-generated method stub

	}

}
