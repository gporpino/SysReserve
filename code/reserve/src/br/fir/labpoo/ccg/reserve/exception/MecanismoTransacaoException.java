package br.fir.labpoo.ccg.reserve.exception;

public class MecanismoTransacaoException extends Exception {

	public MecanismoTransacaoException() {
		// TODO Auto-generated constructor stub
	}

	public MecanismoTransacaoException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public MecanismoTransacaoException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

	public MecanismoTransacaoException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

}
