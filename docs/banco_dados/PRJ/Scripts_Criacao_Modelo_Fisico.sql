CREATE TABLE APARTAMENTO (
  ID_APARTAMENTO                 INTEGER NOT NULL
 ,NUMERO                         VARCHAR(10)
 ,CONSTRAINT PK_APARTAMENTO PRIMARY KEY (ID_APARTAMENTO)
)
;

CREATE GENERATOR GEN_APARTAMENTO;

SET TERM ^ ;
CREATE TRIGGER TR_APARTAMENTO_AI FOR APARTAMENTO BEFORE INSERT AS
  BEGIN
    IF (NEW.ID_APARTAMENTO IS NULL) THEN
      NEW.ID_APARTAMENTO=GEN_ID(GEN_APARTAMENTO, 1);
  END^
SET TERM ; ^

CREATE TABLE CARTAO_FIDELIDADE (
  ID_CARTAO                      INTEGER NOT NULL
 ,ID_HOSPEDE                     INTEGER NOT NULL
 ,NUMERO                         VARCHAR(30)
 ,DATA_GERACAO                   DATE
 ,CONSTRAINT PK_CARTAO_FIDELIDADE PRIMARY KEY (ID_CARTAO)
)
;

CREATE GENERATOR GEN_CARTAO_FIDELIDADE;

SET TERM ^ ;
CREATE TRIGGER TR_CARTAO_FIDELIDADE_AI FOR CARTAO_FIDELIDADE BEFORE INSERT AS
  BEGIN
    IF (NEW.ID_CARTAO IS NULL) THEN
      NEW.ID_CARTAO=GEN_ID(GEN_CARTAO_FIDELIDADE, 1);
  END^
SET TERM ; ^

CREATE INDEX IXFK_CARTAO_FIDELIDADE_1_ID_HO ON CARTAO_FIDELIDADE (ID_HOSPEDE);

CREATE TABLE CIDADE (
  ID_CIDADE                      INTEGER NOT NULL
 ,ID_UF                          INTEGER NOT NULL
 ,NOME                           VARCHAR(60)
 ,CONSTRAINT PK_CIDADE PRIMARY KEY (ID_CIDADE)
)
;

CREATE GENERATOR GEN_CIDADE;

SET TERM ^ ;
CREATE TRIGGER TR_CIDADE_AI FOR CIDADE BEFORE INSERT AS
  BEGIN
    IF (NEW.ID_CIDADE IS NULL) THEN
      NEW.ID_CIDADE=GEN_ID(GEN_CIDADE, 1);
  END^
SET TERM ; ^

CREATE INDEX IXFK_CIDADE_1_ID_UF ON CIDADE (ID_UF);

CREATE TABLE CLIENTE (
  ID_CLIENTE                     INTEGER NOT NULL
 ,ID_PESSOA                      INTEGER NOT NULL
 ,DATA_DCADASTRO                 DATE
 ,CONSTRAINT PK_CLIENTE PRIMARY KEY (ID_CLIENTE)
)
;

CREATE GENERATOR GEN_CLIENTE;

SET TERM ^ ;
CREATE TRIGGER TR_CLIENTE_AI FOR CLIENTE BEFORE INSERT AS
  BEGIN
    IF (NEW.ID_CLIENTE IS NULL) THEN
      NEW.ID_CLIENTE=GEN_ID(GEN_CLIENTE, 1);
  END^
SET TERM ; ^

CREATE INDEX IXFK_CLIENTE_1_ID_PESSOA ON CLIENTE (ID_PESSOA);

CREATE TABLE CONTA (
  ID_CONTA                       INTEGER NOT NULL
 ,ID_HOSPEDAGEM                  INTEGER NOT NULL
 ,CONSTRAINT PK_CONTA PRIMARY KEY (ID_CONTA)
)
;

CREATE GENERATOR GEN_CONTA;

SET TERM ^ ;
CREATE TRIGGER TR_CONTA_AI FOR CONTA BEFORE INSERT AS
  BEGIN
    IF (NEW.ID_CONTA IS NULL) THEN
      NEW.ID_CONTA=GEN_ID(GEN_CONTA, 1);
  END^
SET TERM ; ^

CREATE INDEX IXFK_CONTA_1_ID_HOSPEDAGEM ON CONTA (ID_HOSPEDAGEM);

CREATE TABLE DIARIA (
  ID_DIARIA                      INTEGER NOT NULL
 ,ID_TEMPORADA                   INTEGER NOT NULL
 ,ID_TIPO                        INTEGER NOT NULL
 ,CONSTRAINT PK_DIARIA PRIMARY KEY (ID_DIARIA)
)
;

CREATE GENERATOR GEN_DIARIA;

SET TERM ^ ;
CREATE TRIGGER TR_DIARIA_AI FOR DIARIA BEFORE INSERT AS
  BEGIN
    IF (NEW.ID_DIARIA IS NULL) THEN
      NEW.ID_DIARIA=GEN_ID(GEN_DIARIA, 1);
  END^
SET TERM ; ^

CREATE INDEX IXFK_DIARIA_1_ID_TIPO ON DIARIA (ID_TIPO);

CREATE INDEX IXFK_DIARIA_2_ID_TEMPORADA ON DIARIA (ID_TEMPORADA);

CREATE TABLE ENDERECO (
  ID_ENDERECO                    INTEGER NOT NULL
 ,ID_CIDADE                      INTEGER NOT NULL
 ,LOGRADOURO                     VARCHAR(60)
 ,NUMERO                         INTEGER
 ,CEP                            VARCHAR(10)
 ,COMPLEMENTO                    VARCHAR(45)
 ,BAIRRO                         VARCHAR(45)
 ,CONSTRAINT PK_ENDERECO PRIMARY KEY (ID_ENDERECO)
)
;

CREATE GENERATOR GEN_ENDERECO;

SET TERM ^ ;
CREATE TRIGGER TR_ENDERECO_AI FOR ENDERECO BEFORE INSERT AS
  BEGIN
    IF (NEW.ID_ENDERECO IS NULL) THEN
      NEW.ID_ENDERECO=GEN_ID(GEN_ENDERECO, 1);
  END^
SET TERM ; ^

CREATE INDEX IXFK_ENDERECO_1_ID_CIDADE ON ENDERECO (ID_CIDADE);

CREATE TABLE FUNCIONARIO (
  ID_FUNCIONARIO                 INTEGER NOT NULL
 ,ID_PESSOA                      INTEGER NOT NULL
 ,MATRICULA                      VARCHAR(20)
 ,DATA_ADMISSAO                  DATE
 ,CONSTRAINT PK_FUNCIONARIO PRIMARY KEY (ID_FUNCIONARIO)
)
;

CREATE GENERATOR GEN_FUNCIONARIO;

SET TERM ^ ;
CREATE TRIGGER TR_FUNCIONARIO_AI FOR FUNCIONARIO BEFORE INSERT AS
  BEGIN
    IF (NEW.ID_FUNCIONARIO IS NULL) THEN
      NEW.ID_FUNCIONARIO=GEN_ID(GEN_FUNCIONARIO, 1);
  END^
SET TERM ; ^

CREATE INDEX IXFK_FUNCIONARIO_1_ID_PESSOA ON FUNCIONARIO (ID_PESSOA);

CREATE TABLE HOSPEDAGEM (
  ID_HOSPEDAGEM                  INTEGER NOT NULL
 ,ID_HOSPEDE                     INTEGER NOT NULL
 ,ID_FUNCIONARIO                 INTEGER NOT NULL
 ,DATA_ABERTURA                  DATE
 ,DATA_FECHAMENTO                DATE
 ,CONSTRAINT PK_HOSPEDAGEM PRIMARY KEY (ID_HOSPEDAGEM)
)
;

CREATE GENERATOR GEN_HOSPEDAGEM;

SET TERM ^ ;
CREATE TRIGGER TR_HOSPEDAGEM_AI FOR HOSPEDAGEM BEFORE INSERT AS
  BEGIN
    IF (NEW.ID_HOSPEDAGEM IS NULL) THEN
      NEW.ID_HOSPEDAGEM=GEN_ID(GEN_HOSPEDAGEM, 1);
  END^
SET TERM ; ^

CREATE INDEX IXFK_HOSPEDAGEM_1_ID_HOSPEDE ON HOSPEDAGEM (ID_HOSPEDE);

CREATE INDEX IXFK_HOSPEDAGEM_2_ID_FUNCIONAR ON HOSPEDAGEM (ID_FUNCIONARIO);

CREATE TABLE HOSPEDAGEM_APARTAMENTO (
  ID_APARTAMENTO                 INTEGER NOT NULL
 ,ID_HOSPEDAGEM                  INTEGER NOT NULL
 ,CONSTRAINT PK_HOSPEDAGEM_APARTAMENTO PRIMARY KEY (ID_APARTAMENTO, ID_HOSPEDAGEM)
)
;

CREATE INDEX IXFK_HOSPEDAGEM_APARTAMENTO_1_ ON HOSPEDAGEM_APARTAMENTO (ID_HOSPEDAGEM);

CREATE INDEX IXFK_HOSPEDAGEM_APARTAMENTO_2_ ON HOSPEDAGEM_APARTAMENTO (ID_APARTAMENTO);

CREATE TABLE HOSPEDE (
  ID_HOSPEDE                     INTEGER NOT NULL
 ,ID_CLIENTE                     INTEGER NOT NULL
 ,CONSTRAINT PK_HOSPEDE PRIMARY KEY (ID_HOSPEDE)
)
;

CREATE GENERATOR GEN_HOSPEDE;

SET TERM ^ ;
CREATE TRIGGER TR_HOSPEDE_AI FOR HOSPEDE BEFORE INSERT AS
  BEGIN
    IF (NEW.ID_HOSPEDE IS NULL) THEN
      NEW.ID_HOSPEDE=GEN_ID(GEN_HOSPEDE, 1);
  END^
SET TERM ; ^

CREATE INDEX IXFK_HOSPEDE_1_ID_CLIENTE ON HOSPEDE (ID_CLIENTE);

CREATE TABLE PESSOA (
  ID_PESSOA                      INTEGER NOT NULL
 ,ID_ENDERECO                    INTEGER NOT NULL
 ,NOME                           VARCHAR(60)
 ,CPF                            VARCHAR(11)
 ,SEXO                           CHAR(1) CHECK(SEXO IN('M', 'F'))
 ,DATA_NASCIMENTO                DATE
 ,CONSTRAINT PK_PESSOA PRIMARY KEY (ID_PESSOA)
)
;

CREATE GENERATOR GEN_PESSOA;

SET TERM ^ ;
CREATE TRIGGER TR_PESSOA_AI FOR PESSOA BEFORE INSERT AS
  BEGIN
    IF (NEW.ID_PESSOA IS NULL) THEN
      NEW.ID_PESSOA=GEN_ID(GEN_PESSOA, 1);
  END^
SET TERM ; ^

CREATE INDEX IXFK_PESSOA_1_ID_ENDERECO ON PESSOA (ID_ENDERECO);

CREATE TABLE RESERVA (
  ID_RESERVA                     INTEGER NOT NULL
 ,ID_CLIENTE                     INTEGER NOT NULL
 ,DATA_ABERTURA                  DATE
 ,DATA_FECHAMENTO                DATE
 ,CONSTRAINT PK_RESERVA PRIMARY KEY (ID_RESERVA)
)
;

CREATE GENERATOR GEN_RESERVA;

SET TERM ^ ;
CREATE TRIGGER TR_RESERVA_AI FOR RESERVA BEFORE INSERT AS
  BEGIN
    IF (NEW.ID_RESERVA IS NULL) THEN
      NEW.ID_RESERVA=GEN_ID(GEN_RESERVA, 1);
  END^
SET TERM ; ^

CREATE INDEX IXFK_RESERVA_1_ID_CLIENTE ON RESERVA (ID_CLIENTE);

CREATE TABLE RESERVA_APARTAMENTO (
  ID_RESERVA                     INTEGER NOT NULL
 ,ID_APARTAMENTO                 INTEGER NOT NULL
 ,CONSTRAINT PK_RESERVA_APARTAMENTO PRIMARY KEY (ID_RESERVA, ID_APARTAMENTO)
)
;

CREATE INDEX IXFK_RESERVA_APARTAMENTO_1_ID_ ON RESERVA_APARTAMENTO (ID_RESERVA);

CREATE INDEX IXFK_RESERVA_APARTAMENTO_2_ID_ ON RESERVA_APARTAMENTO (ID_APARTAMENTO);

CREATE TABLE SERVICO (
  ID_SERVICO                     INTEGER NOT NULL
 ,DESCRICAO                      VARCHAR(60)
 ,VALOR                          NUMERIC(15,2)
 ,CONSTRAINT PK_SERVICO PRIMARY KEY (ID_SERVICO)
)
;

CREATE GENERATOR GEN_SERVICO;

SET TERM ^ ;
CREATE TRIGGER TR_SERVICO_AI FOR SERVICO BEFORE INSERT AS
  BEGIN
    IF (NEW.ID_SERVICO IS NULL) THEN
      NEW.ID_SERVICO=GEN_ID(GEN_SERVICO, 1);
  END^
SET TERM ; ^

CREATE TABLE SERVICO_CONTA (
  ID_CONTA                       INTEGER NOT NULL
 ,ID_SERVICO                     INTEGER NOT NULL
 ,CONSTRAINT PK_SERVICO_CONTA PRIMARY KEY (ID_CONTA, ID_SERVICO)
)
;

CREATE INDEX IXFK_SERVICO_CONTA_1_ID_SERVIC ON SERVICO_CONTA (ID_SERVICO);

CREATE INDEX IXFK_SERVICO_CONTA_2_ID_CONTA ON SERVICO_CONTA (ID_CONTA);

CREATE TABLE TELEFONE (
  ID_TELEFONE                    INTEGER NOT NULL
 ,ID_TIPO_TELEFONE               INTEGER NOT NULL
 ,CODIGO_DDD                     VARCHAR(4)
 ,NUMERO                         VARCHAR(10)
 ,CONSTRAINT PK_TELEFONE PRIMARY KEY (ID_TELEFONE)
)
;

CREATE GENERATOR GEN_TELEFONE;

SET TERM ^ ;
CREATE TRIGGER TR_TELEFONE_AI FOR TELEFONE BEFORE INSERT AS
  BEGIN
    IF (NEW.ID_TELEFONE IS NULL) THEN
      NEW.ID_TELEFONE=GEN_ID(GEN_TELEFONE, 1);
  END^
SET TERM ; ^

CREATE INDEX IXFK_TELEFONE_1_ID_TIPO_TELEFO ON TELEFONE (ID_TIPO_TELEFONE);

CREATE TABLE TELEFONE_PESSOA (
  ID_TELEFONE                    INTEGER NOT NULL
 ,ID_PESSOA                      INTEGER NOT NULL
 ,CONSTRAINT PK_TELEFONE_PESSOA PRIMARY KEY (ID_TELEFONE, ID_PESSOA)
)
;

CREATE INDEX IXFK_TELEFONE_PESSOA_1_ID_TELE ON TELEFONE_PESSOA (ID_TELEFONE);

CREATE INDEX IXFK_TELEFONE_PESSOA_2_ID_PESS ON TELEFONE_PESSOA (ID_PESSOA);

CREATE TABLE TEMPORADA (
  ID_TEMPORADA                   INTEGER NOT NULL
 ,DATA_INICIO                    DATE
 ,DATA_FIM                       DATE
 ,VALOR                          NUMERIC(15,2)
 ,CONSTRAINT PK_TEMPORADA PRIMARY KEY (ID_TEMPORADA)
)
;

CREATE GENERATOR GEN_TEMPORADA;

SET TERM ^ ;
CREATE TRIGGER TR_TEMPORADA_AI FOR TEMPORADA BEFORE INSERT AS
  BEGIN
    IF (NEW.ID_TEMPORADA IS NULL) THEN
      NEW.ID_TEMPORADA=GEN_ID(GEN_TEMPORADA, 1);
  END^
SET TERM ; ^

CREATE TABLE TIPO (
  ID_TIPO                        INTEGER NOT NULL
 ,DESCRICAO                      VARCHAR(60)
 ,CONSTRAINT PK_TIPO PRIMARY KEY (ID_TIPO)
)
;

CREATE GENERATOR GEN_TIPO;

SET TERM ^ ;
CREATE TRIGGER TR_TIPO_AI FOR TIPO BEFORE INSERT AS
  BEGIN
    IF (NEW.ID_TIPO IS NULL) THEN
      NEW.ID_TIPO=GEN_ID(GEN_TIPO, 1);
  END^
SET TERM ; ^

CREATE TABLE TIPO_APARTAMENTO (
  ID_TIPO                        INTEGER NOT NULL
 ,ID_APARTAMENTO                 INTEGER NOT NULL
 ,CONSTRAINT PK_TIPO_APARTAMENTO PRIMARY KEY (ID_TIPO, ID_APARTAMENTO)
)
;

CREATE INDEX IXFK_TIPO_APARTAMENTO_1_ID_TIP ON TIPO_APARTAMENTO (ID_TIPO);

CREATE INDEX IXFK_TIPO_APARTAMENTO_2_ID_APA ON TIPO_APARTAMENTO (ID_APARTAMENTO);

CREATE TABLE TIPO_TELEFONE (
  ID_TIPO_TELEFONE               INTEGER NOT NULL
 ,DESCRICAO                      VARCHAR(60)
 ,CONSTRAINT PK_TIPO_TELEFONE PRIMARY KEY (ID_TIPO_TELEFONE)
)
;

CREATE GENERATOR GEN_TIPO_TELEFONE;

SET TERM ^ ;
CREATE TRIGGER TR_TIPO_TELEFONE_AI FOR TIPO_TELEFONE BEFORE INSERT AS
  BEGIN
    IF (NEW.ID_TIPO_TELEFONE IS NULL) THEN
      NEW.ID_TIPO_TELEFONE=GEN_ID(GEN_TIPO_TELEFONE, 1);
  END^
SET TERM ; ^

CREATE TABLE UNIDADE_FEDERATIVA (
  ID_UF                          INTEGER NOT NULL
 ,NOME                           VARCHAR(2)
 ,CONSTRAINT PK_UNIDADE_FEDERATIVA PRIMARY KEY (ID_UF)
)
;

CREATE GENERATOR GEN_UNIDADE_FEDERATIVA;

SET TERM ^ ;
CREATE TRIGGER TR_UNIDADE_FEDERATIVA_AI FOR UNIDADE_FEDERATIVA BEFORE INSERT AS
  BEGIN
    IF (NEW.ID_UF IS NULL) THEN
      NEW.ID_UF=GEN_ID(GEN_UNIDADE_FEDERATIVA, 1);
  END^
SET TERM ; ^

ALTER TABLE CARTAO_FIDELIDADE
  ADD CONSTRAINT FK_CARTAO_FIDELIDADE_HOSPEDE FOREIGN KEY (ID_HOSPEDE)
    REFERENCES HOSPEDE (ID_HOSPEDE)
;
ALTER TABLE CIDADE
  ADD CONSTRAINT FK_CIDADE_UNIDADE_FEDERATIVA FOREIGN KEY (ID_UF)
    REFERENCES UNIDADE_FEDERATIVA (ID_UF)
;
ALTER TABLE CLIENTE
  ADD CONSTRAINT FK_CLIENTE_PESSOA FOREIGN KEY (ID_PESSOA)
    REFERENCES PESSOA (ID_PESSOA)
;
ALTER TABLE CONTA
  ADD CONSTRAINT FK_CONTA_HOSPEDAGEM FOREIGN KEY (ID_HOSPEDAGEM)
    REFERENCES HOSPEDAGEM (ID_HOSPEDAGEM)
;
ALTER TABLE DIARIA
  ADD CONSTRAINT FK_DIARIA_TIPO FOREIGN KEY (ID_TIPO)
    REFERENCES TIPO (ID_TIPO)
;
ALTER TABLE DIARIA
  ADD CONSTRAINT FK_DIARIA_TEMPORADA FOREIGN KEY (ID_TEMPORADA)
    REFERENCES TEMPORADA (ID_TEMPORADA)
;
ALTER TABLE ENDERECO
  ADD CONSTRAINT FK_ENDERECO_CIDADE FOREIGN KEY (ID_CIDADE)
    REFERENCES CIDADE (ID_CIDADE)
;
ALTER TABLE FUNCIONARIO
  ADD CONSTRAINT FK_FUNCIONARIO_PESSOA FOREIGN KEY (ID_PESSOA)
    REFERENCES PESSOA (ID_PESSOA)
;
ALTER TABLE HOSPEDAGEM
  ADD CONSTRAINT FK_HOSPEDAGEM_HOSPEDE FOREIGN KEY (ID_HOSPEDE)
    REFERENCES HOSPEDE (ID_HOSPEDE)
;
ALTER TABLE HOSPEDAGEM
  ADD CONSTRAINT FK_HOSPEDAGEM_FUNCIONARIO FOREIGN KEY (ID_FUNCIONARIO)
    REFERENCES FUNCIONARIO (ID_FUNCIONARIO)
;
ALTER TABLE HOSPEDAGEM_APARTAMENTO
  ADD CONSTRAINT FK_HOSPEDAGEM_APARTAMENTO_HOSP FOREIGN KEY (ID_HOSPEDAGEM)
    REFERENCES HOSPEDAGEM (ID_HOSPEDAGEM)
;
ALTER TABLE HOSPEDAGEM_APARTAMENTO
  ADD CONSTRAINT FK_HOSPEDAGEM_APARTAMENTO_APAR FOREIGN KEY (ID_APARTAMENTO)
    REFERENCES APARTAMENTO (ID_APARTAMENTO)
;
ALTER TABLE HOSPEDE
  ADD CONSTRAINT FK_HOSPEDE_CLIENTE FOREIGN KEY (ID_CLIENTE)
    REFERENCES CLIENTE (ID_CLIENTE)
;
ALTER TABLE PESSOA
  ADD CONSTRAINT FK_PESSOA_ENDERECO FOREIGN KEY (ID_ENDERECO)
    REFERENCES ENDERECO (ID_ENDERECO)
;
ALTER TABLE RESERVA
  ADD CONSTRAINT FK_RESERVA_CLIENTE FOREIGN KEY (ID_CLIENTE)
    REFERENCES CLIENTE (ID_CLIENTE)
;
ALTER TABLE RESERVA_APARTAMENTO
  ADD CONSTRAINT FK_RESERVA_APARTAMENTO_RESERVA FOREIGN KEY (ID_RESERVA)
    REFERENCES RESERVA (ID_RESERVA)
;
ALTER TABLE RESERVA_APARTAMENTO
  ADD CONSTRAINT FK_RESERVA_APARTAMENTO_APARTAM FOREIGN KEY (ID_APARTAMENTO)
    REFERENCES APARTAMENTO (ID_APARTAMENTO)
;
ALTER TABLE SERVICO_CONTA
  ADD CONSTRAINT FK_SERVICO_CONTA_SERVICO FOREIGN KEY (ID_SERVICO)
    REFERENCES SERVICO (ID_SERVICO)
;
ALTER TABLE SERVICO_CONTA
  ADD CONSTRAINT FK_SERVICO_CONTA_CONTA FOREIGN KEY (ID_CONTA)
    REFERENCES CONTA (ID_CONTA)
;
ALTER TABLE TELEFONE
  ADD CONSTRAINT FK_TELEFONE_TIPO_TELEFONE FOREIGN KEY (ID_TIPO_TELEFONE)
    REFERENCES TIPO_TELEFONE (ID_TIPO_TELEFONE)
;
ALTER TABLE TELEFONE_PESSOA
  ADD CONSTRAINT FK_TELEFONE_PESSOA_TELEFONE FOREIGN KEY (ID_TELEFONE)
    REFERENCES TELEFONE (ID_TELEFONE)
;
ALTER TABLE TELEFONE_PESSOA
  ADD CONSTRAINT FK_TELEFONE_PESSOA_PESSOA FOREIGN KEY (ID_PESSOA)
    REFERENCES PESSOA (ID_PESSOA)
;
ALTER TABLE TIPO_APARTAMENTO
  ADD CONSTRAINT FK_TIPO_APARTAMENTO_TIPO FOREIGN KEY (ID_TIPO)
    REFERENCES TIPO (ID_TIPO)
;
ALTER TABLE TIPO_APARTAMENTO
  ADD CONSTRAINT FK_TIPO_APARTAMENTO_APARTAMENT FOREIGN KEY (ID_APARTAMENTO)
    REFERENCES APARTAMENTO (ID_APARTAMENTO)
;
